#!/bin/bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


function process() {
  # export variables to use these in docker-container
  exportDataForDocker

  # install Docker
  installDockerCE

  #install composer
  installComposer

  #clone the repo and install magento
  cloneAndBuildDocker

  #install_magento

  showDatabaseLogin

  installMagentoExtentions
}

#docker exec -ti apache2 bash

## Get input from User
function getUserInput {
  clear
  printf "\nFor the next steps some information has to be collected, \nplease confirm with Enter when you are ready.\n\n"
  read -n 1 -s -r

  printf "\nFor the required repo we need a password, which must be entered for security reasons\n"
  read -p "Bitbucket Repo password: " bitbucket_password

  printf "\n\nCollect Magneto informations\n\n"
  read -p "Magento IP-Address or URL: " magento_ip_uri
  
  printf "\n\nCollect Database information\n"
  read -p "Database Name: " database_name
  read -p "Database User: " database_user
  read -s -p "Database Password: " database_password
  askDataCorrect
}

function installDockerCE {
  printf "\n**************************************\n**      Install Docker CE           **\n**************************************\n\n"
  apt-get remove docker docker-engine docker.io
  apt-get update
  apt-get install apt-transport-https ca-certificates curl software-properties-common -y
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  apt-key fingerprint 0EBFCD88
  add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  apt-get update
  apt-get install docker-ce git sshpass -y
}

function installComposer {
  curl -L "https://github.com/docker/compose/releases/download/1.21.0-rc1/docker-compose-$(uname -s)-$(uname -m)" > ./docker-compose
  mv ./docker-compose /usr/bin/docker-compose
  chmod +x /usr/bin/docker-compose
}

function cloneAndBuildDocker {
#/root/.composer/auth.json'

  cd ~/ 

  git clone https://chriss1983:$bitbucket_password@bitbucket.org/chriss1983projecs/magento2ssl.git
  #git clone https://redeyedex:$bitbucket_password@bitbucket.org/redeyedex/magento2_ssl.git
  
  cd magento2ssl

  database_user_password=`date | md5sum | fold -w 12 | head -n 1`

  # change ip in ssl_server/default
  sed -i "s/IPADDRESS/${magento_ip_uri}/g" ssl_server/default

  # change data for database in database_server/mysql.sh
  sed -i "s/DATABASENAME/${database_name}/g" database_server/mysql.sh
  sed -i "s/DATABASEUSER/${database_user}/g" database_server/mysql.sh
  sed -i "s/DATABASEROOT/${database_password}/g" database_server/mysql.sh

  # change data for database in docker-compose
  sed -i "s/DATABASENAME/${database_name}/g" docker-compose.yml
  sed -i "s/DATABASEUSER/${database_user}/g" docker-compose.yml
  sed -i "s/DATABASEROOT/${database_password}/g" docker-compose.yml

  mkdir ~/magento2ssl/magento2

  cp ~/magento2ssl/web_server/auth.json /root/.composer/

  apt-get -y install php7.0 php7.0-curl php7.0-intl php7.0-gd php7.0-dom php7.0-mcrypt php7.0-iconv php7.0-xsl php7.0-mbstring php7.0-ctype php7.0-zip php7.0-pdo php7.0-xml php7.0-bz2 php7.0-calendar php7.0-exif php7.0-fileinfo php7.0-json php7.0-mysqli php7.0-mysql php7.0-posix php7.0-tokenizer php7.0-xmlwriter php7.0-xmlreader php7.0-phar php7.0-soap php7.0-mysql php7.0-fpm libapache2-mod-php7.0

  docker-compose build

  composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition ~/magento2ssl/magento2

  find ~/magento2ssl/magento2 -type d -exec chmod 700 {} \; && find ~/magento2ssl/magento2 -type f -exec chmod 600 {} \;

  chown -R www-data ~/magento2ssl/magento2

  docker-compose up -d

  docker exec -i mysql cat /var/log/check.log
}

function installMagentoExtentions {
  printf "\n***** install DE language pack *****\n"
  cd ~/magento2ssl/magento2
  composer require mageplaza/magento-2-german-language-pack:dev-master
  php ./bin/magento setup:static-content:deploy de_DE
  php ./bin/magento cache:flush
}

function exportDataForDocker {
  export MAGENTO_IP_URI=${magento_ip_uri}

  export DB_NAME=${database_name}
  export DB_USER=${database_user}
  export DB_PASSWORD=${database_password}
}

function askDataCorrect {
  clear
  printf "\n\nare these informations correct?:\n"
  printf "Bitbucket Password: $bitbucket_password"
  printf "\nMagneto URL/IP : $magento_ip_uri"

  printf "\nDatabase Name: $database_name"
  printf "\nDatabase Username: $database_user"
  printf "\nDatabase Root Password: $database_password\n\n"

  read -p "are the informations correct? (y/n)?" choice
  case "$choice" in 
    y|Y ) process;;
    n|N ) getUserInput;;
    * ) echo "invalid";;
  esac
}

#funtion install_magento {

#}

function showDatabaseLogin {
  read -n 1 -s -r -p "Please write down the credencials!
  otherwise you can get these infos later by enter:
  docker exec -i mysql cat /var/log/check.log

  now open your browser and enter $magento_ip_uri
  follow the setup, and when you done, come back and press return.

  press enter when you ready . . "
}

clear

printf "\n\n
***************************************
** Welcome to Magento auto Installer **
***************************************\n\n"

getUserInput